<?php

namespace Adminows\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin_home')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getManager(User::class);
        
        return $this->render('home/index.html.twig', [
        ]);
    }
}
