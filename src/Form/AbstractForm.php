<?php

namespace App\Form;

use Exception;
use Symfony\Component\Form\AbstractType;

abstract class AbstractForm extends AbstractType
{
    /**
     * Make floating function.
     *
     * @param string $label
     * @return array
     */
    protected function makeFloating(string $label, array $options = []): array
    {
        if (!trim($label)) {
            throw new Exception('Le label est requis', 1);
        }

        $floatingOptions = [
            'label' => $label,
            'attr' => ['placeholder' => $label],
            'row_attr' => ['class' => 'form-floating mb-3']
        ];

        if (isset($options['label'])) {
            unset($options['label']);
        }

        if (isset($options['attr']) && isset($options['attr']['placeholder'])) {
            unset($options['attr']['placeholder']);
        }

        if (isset($options['row_attr']) && isset($options['row_attr']['class'])) {
            $floatingOptions['row_attr']['class'] .= ' ' . $options['row_attr']['class'];
            unset($options['row_attr']['class']);
        }

        return array_merge_recursive($floatingOptions, $options);
    }

}